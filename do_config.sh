#!/usr/bin/env bash

# enable i3
# dconf write /org/mate/desktop/session/required-components/windowmanager "'i3'"

# disable desktop icons
dconf write /org/mate/desktop/background/show-desktop-icons "false"
dconf write /org/mate/desktop/background/draw-background "false"
