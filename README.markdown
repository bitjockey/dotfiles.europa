This is my dotfiles repo. I use `i3-gaps` with MATE desktop.

## Setup

```
git clone https://gitlab.com/bitjockey/dotfiles.europa.git dotfiles
cd dotfiles
git submodule update --init --recursive
```

## MATE changes

You need to prevent MATE desktop from drawing the background:

```
dconf write /org/mate/desktop/background/draw-background false
```

## Dependencies

- [xcb-util-xrm](https://github.com/Airblader/xcb-util-xrm)
- [i3-gaps](https://github.com/Airblader/i3/wiki/Building-from-source)
- [i3status-rs](https://github.com/greshake/i3status-rust)
- [picom](https://github.com/yshui/picom)
- [urxvt](https://github.com/exg/rxvt-unicode)
- [feh](https://wiki.archlinux.org/index.php/Feh)
- [rofi](https://github.com/davatorium/rofi)
- [conky](https://github.com/brndnmtthws/conky)

**NOTE** You'll have to replace `gdm` with `lightdm`.

```
sudo apt update -y && sudo apt install -y lightdm
```

## Fonts

Main fonts are:

- [United Sans Reg](https://www.wfonts.com/font/united-sans-reg)
- [Input Mono (Envy Code R style)](https://input.fontbureau.com/download/?customize&fontSelection=whole&a=0&g=ss&i=topserif&l=topserif&zero=slash&asterisk=0&braces=0&preset=envy&line-height=1.2&accept=I+do&email=)


Additional ones for icons:

```
sudo apt install fonts-font-awesome
```

Will also need the patched [material-icons](https://gist.github.com/draoncc/3c20d8d4262892ccd2e227eefeafa8ef/raw/3e6e12c213fba1ec28aaa26430c3606874754c30/MaterialIcons-Regular-for-inline.ttf) font, from i3status-rs.

## Additional Configs

- [edex-ui-conky by Sephiroth-XIII](https://github.com/Sephiroth-XIII/edex-ui-conky)

## Preview

![](screenshots/preview.png)