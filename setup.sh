#!/usr/bin/env bash

install_build_deps() {
    sudo apt update -y
    sudo apt install -y meson ninja-build intltool libtool stow
}

install_xcb_util_xrm() {
    # dependencies
    sudo apt update -y
    sudo apt install -y libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev xutils-dev libxcb-shape0-dev autoconf
    # Install
    cd /tmp
    git clone https://github.com/Airblader/xcb-util-xrm
    cd xcb-util-xrm
    git submodule update --init
    ./autogen.sh --prefix=/usr
    make
    sudo make install
}

install_i3_gaps() {
    # Dependencies
    sudo apt update -y
    sudo apt install -y libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev \
    libxcb-util0-dev libxcb-icccm4-dev libyajl-dev \
    libstartup-notification0-dev libxcb-randr0-dev \
    libev-dev libxcb-cursor-dev libxcb-xinerama0-dev \
    libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev \
    autoconf libxcb-xrm0 libxcb-xrm-dev automake libxcb-shape0-dev
    # Install
    cd /tmp
    git clone https://www.github.com/Airblader/i3 i3-gaps
    cd i3-gaps
    mkdir -p build && cd build
    meson ..
    ninja
    meson install
    # Install desktop files
    cd /tmp/i3-gaps
    sudo cp share/applications/*.desktop /usr/share/applications/
    sudo cp share/xsessions/*.desktop /usr/share/xsessions/
}

install_i3_rust() {
    # https://doc.rust-lang.org/cargo/getting-started/installation.html
    # Setup rust
    curl https://sh.rustup.rs -sSf | sh
    source $HOME/.cargo/env

    # Install dependencies
    sudo apt install -y libdbus-glib-1-dev libssl-dev
    cargo install --git https://github.com/greshake/i3status-rust i3status-rs
}

install_picom() {
    sudo apt install libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev libpcre2-dev libpcre3-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev
    mkdir -p ~/.code/
    cd ~/.code
    git clone https://github.com/yshui/picom.git
    cd picom
    git submodule update --init --recursive
    meson --buildtype=release . build
    ninja -C build
    ninja -C build install
}

install() {
    install_build_deps
    install_xcb_util_xrm
    install_i3_gaps
    install_i3_rust
    install_picom
}

